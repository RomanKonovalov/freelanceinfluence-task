import { computed, Ref, ref } from 'vue'
import { UserPublicInfo } from '@/types/user.model'
import { TaskPublicInfo } from '@/types/task.model'

const myProfile: Ref<UserPublicInfo | null> = ref(null)
const myTask: Ref<TaskPublicInfo | null> = ref(null)

export default function useUserStore() {
  const getMyProfile = computed(() => myProfile.value)
  const setMyProfile = (data: UserPublicInfo | null) => {
    myProfile.value = data
  }
  
  const getMyTask = computed(() => myTask.value)
  const setMyTask = (data: TaskPublicInfo | null) => {
    myTask.value = data
  }

  const reset = () => {
    setMyProfile(null)
    setMyTask(null)
  }

  return {
    getMyProfile,
    setMyProfile,
    getMyTask,
    setMyTask,
    reset,
  }
}
