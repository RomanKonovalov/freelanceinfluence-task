import { ref, watch } from 'vue'

const AUTH_ITEM = 'isUserAuthenticated'
const USER_ID_ITEM = 'userId'
const isAuthenticated = ref(false)
const userIdLocalStorage = ref('')

const _updateAuthStateInLS = () => {
  localStorage.setItem(AUTH_ITEM, JSON.stringify(isAuthenticated.value))
  localStorage.setItem(USER_ID_ITEM, userIdLocalStorage.value)
}

const _loadAuthStateFromLS = () => {
  const ls = localStorage.getItem(AUTH_ITEM)
  const ui = localStorage.getItem(USER_ID_ITEM)

  if (ls && typeof JSON.parse(ls) === 'boolean') isAuthenticated.value = JSON.parse(ls)
  if (ui) userIdLocalStorage.value = ui
  else _updateAuthStateInLS()
}

const getLocalStorageData = () => {
  return localStorage.getItem(USER_ID_ITEM)
}

_loadAuthStateFromLS()
watch(isAuthenticated, _updateAuthStateInLS)

const reset = () => {
  isAuthenticated.value = false
}

export default function useAuthStore() {
  return {
    isAuthenticated,
    userIdLocalStorage,
    reset,
    getLocalStorageData
  }
}
