import { CurrencyValue } from './money.model'

export interface TaskPublicInfo {
  title: string
  description: string
  budget: CurrencyValue
  platforms: Array<string>
  filesIds: Array<string>
}