import api from '@/services/api'
import useUserStore from '@/store/user'
import { UserPublicInfo } from '@/types/user.model'

const { setMyProfile } = useUserStore()

const setUserById = async (id: string) => {
  return await api.get('users/' + id).then((response) => {
    const userprofiledata = <UserPublicInfo>response.data
    setMyProfile(userprofiledata)
    return response
  })
}

export default function userManagementController() {
  return {
    setUserById
  }
}
