import api from '@/services/api'
import useUserStore from '@/store/user'
import { TaskPublicInfo } from '@/types/task.model'

const { setMyTask } = useUserStore()

const createTask = async (task: TaskPublicInfo) => {
  return await api.post('tasks', task).then((response) => {
    setMyTask(response.data)
    return response
  })
}

export default function taskController() {
  return {
    createTask
  }
}
