import api from '@/services/api'
import useUserStore from '@/store/user'
import { UserResponseWithJWT, UserToLogin } from '@/types/user.model'
import useAuthStore from '@/store/auth'
import useResetStore from '@/store/reset'

const { setMyProfile, setMyTask } = useUserStore()
const auth = useAuthStore()
const reset = useResetStore()
import { TaskPublicInfo } from '@/types/task.model'

const login = async (user: UserToLogin) => {
  return await api.post<UserResponseWithJWT>('users/auth', user).then(async (response) => {
    auth.isAuthenticated.value = true
    auth.userIdLocalStorage.value = response.data.user.id
    setMyProfile(response.data.user)
    await api.get('tasks/my?limit=5').then((response) => {
      const taskdata = <TaskPublicInfo>response.data
      setMyTask(taskdata)
      return;
    })
  })
}

const logout = async () => {
  return await api.post<Record<string, never>>('users/auth/logout').finally(() => {
    reset.reset()
  })
}

export default function useUserAuthentificationController() {
  return {
    login,
    logout,
  }
}
