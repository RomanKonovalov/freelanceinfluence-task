import { reactive } from 'vue'
import { useRouter } from 'vue-router'
import taskController from '@/controllers/taskController'
import { TaskPublicInfo } from '@/types/task.model'
import { Currency, CurrencyValue } from '@/types/money.model'

export default function createRow() {
  const taskCont = taskController()
  const router = useRouter()

  const task = reactive<TaskPublicInfo>({
    title: '',
    description: '',
    budget: {value:0, currency: Currency.USD},
    platforms: [""],
    filesIds: [""]
  })

  const clear = () => {
    task.title = ''
    task.description = ''
    task.budget = {value:23, currency: Currency.USD}
    task.platforms = []
    task.filesIds = [""]
  }

  const validate = (task: TaskPublicInfo) => {
    return task.title.length >= 1 && task.budget.value && task.platforms
  }

  const createTask = () => {
    if (validate(task)) {
      taskCont
        .createTask(task)
        .then((response) => {
          router.push({ name: 'Dashboard' })
        })
        .catch(() => {
          clear()
        })
    }
  }

  return {
    task,
    createTask,
    clear,
  }
}
