import { useRouter } from 'vue-router'
import userManagementController from '@/controllers/userManagementController'
import taskController from '@/controllers/taskController'

export default function useUserData() {
  const user = userManagementController()
  const task = taskController()
  const router = useRouter()

  const setUserStore = (id: string) => {
    user
      .setUserById(id)
      .then((data) => {
        // console.log(data)
      })
      .catch(() => {
        console.log("error")
      })
  }

  return {
    setUserStore
  }
}
